const keyId = 'id';
const keyName = 'pizzaName';
const keyDescription = 'description';
const keyPrice = 'price';
const keyImage = 'imageUrl';

class Pizza {
  late int id;
  late String pizzaName;
  late String description;
  late double price;
  late String imageUrl;

  Pizza.fromJson(Map<String, dynamic> json) {
    this.id = json['id'] ?? 0;
    this.pizzaName = json[keyName] ?? '';
    this.description = json[keyDescription] ?? '';
    this.price = json[keyPrice] ?? 0.0;
    this.imageUrl = json[keyImage] ?? '';
  }
}