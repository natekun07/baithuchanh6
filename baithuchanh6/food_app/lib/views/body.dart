import 'dart:convert';

import 'package:flutter/material.dart';

import '../models/pizza.dart';


class body extends StatefulWidget {
  @override
  _bodyState createState() => _bodyState();
}

class _bodyState extends State<body>{
  @override
  void initState() {
    readJsonFile();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder(
        future: readJsonFile(),
        builder: (BuildContext context, AsyncSnapshot<List<Pizza>> pizzas) {
          return ListView.builder(
              itemCount: pizzas.data?.length ?? 0,
              itemBuilder: (BuildContext context, int index) {
                var itemData = pizzas.data![index];
                return ListTile(
                  title: Text(itemData.pizzaName),
                  subtitle: Text("${itemData.description } \$ ${itemData.price}"),
                );
              }
          );
        },
      ),
    );
  }
  Future<List<Pizza>> readJsonFile() async {
    String myString = await DefaultAssetBundle.of(context).loadString('assets/pizza_list.json');

    List myMap = jsonDecode(myString);
    List<Pizza> myPizzas = [];
    myMap.forEach((dynamic pizza) {
      Pizza myPizza = Pizza.fromJson(pizza);
      myPizzas.add(myPizza);
    });

    return myPizzas;
  }
}